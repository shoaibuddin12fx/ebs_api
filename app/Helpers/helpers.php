<?php

use App\Models\OtpNumber;

if (!function_exists('setting')) {
    function setting($key, $default = null)
    {
        return TCG\Voyager\Facades\Voyager::setting($key, $default);
    }
}

if (!function_exists('menu')) {
    function menu($menuName, $type = null, array $options = [])
    {
        return []; //TCG\Voyager\Facades\Voyager::model('Menu')->display($menuName, $type, $options);
    }
}

if (!function_exists('voyager_asset')) {
    function voyager_asset($path, $secure = null)
    {
        return route('voyager.voyager_assets').'?path='.urlencode($path);
    }
}

if (!function_exists('get_file_name')) {
    function get_file_name($name)
    {
        preg_match('/(_)([0-9])+$/', $name, $matches);
        if (count($matches) == 3) {
            return Illuminate\Support\Str::replaceLast($matches[0], '', $name).'_'.(intval($matches[2]) + 1);
        } else {
            return $name.'_1';
        }
    }

}
if (!function_exists('incrementalHash')) {

    function incrementalHash($num = 6)
    {
        $digit_random_number = $num == 4 ? mt_rand(1000, 9999) : mt_rand(100000, 999999);
        return $digit_random_number;
    }
}

if (!function_exists('generate_otp')) {

      function generate_otp(){
          $num = 6;
          $hash = $num == 4 ? mt_rand(1000, 9999) : mt_rand(100000, 999999);
          while(OtpNumber::where('otp_code', $hash)->first()){
              $hash = $num == 4 ? mt_rand(1000, 9999) : mt_rand(100000, 999999);
          }

        return $hash;
    }

}


