<?php

// Code within app\Helpers\Helper.php

namespace App\Helpers;

use App\Models\OtpNumber;

use Illuminate\Support\Facades\Storage;

class GlobalHelpers
{
    public static function incrementalHash($num = 6) {
        $digit_random_number = $num == 4 ? mt_rand(1000, 9999) : mt_rand(100000, 999999) ;
        return $digit_random_number;
    }

    public static function generateOtp(){
        $hash = GlobalHelpers::incrementalHash();
        while(OtpNumber::where('otp_code', $hash)->first()){
            $hash = GlobalHelpers::incrementalHash();
        }

        return $hash;
    }

    public static function generateRolesObject($role){
        $role = GlobalHelpers::incrementalHash();
        while(OtpNumber::where('otp_code', $hash)->first()){
            $hash = GlobalHelpers::incrementalHash();
        }

        return $hash;
    }



}



